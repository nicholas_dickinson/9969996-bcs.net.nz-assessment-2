﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace assessment_9969996
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        public MainPage()
        {
            this.InitializeComponent();
            populateCombo();

        }



        private async void sendBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Windows.UI.Popups.MessageDialog(
            "To: "   + toBox.Text + "\n" +
            "From: " + fromBox.Text + "\n" +
            "Subject: " + subjectCombo.SelectedItem + "\n" +
            "Message: " + messageBox.Text + "\n");
            dialog.Title = "Email";

            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Send") { Id = 0 });
            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Clear") { Id = 2 });
            

            dialog.DefaultCommandIndex = 0;
            dialog.CancelCommandIndex = 1;


            var result = await dialog.ShowAsync();

            if((int)result.Id == 0)
            {
                MessageDialog sent = new MessageDialog("Your Message has been successful sent!");
                await sent.ShowAsync();
            }
            else if((int)result.Id == 2)
            {
                clearData();
            }
        }

        /*Start Programming Logic*/

        private void clearData()
        {
            toBox.Text = "";
            fromBox.Text = "";
            messageBox.Text = "";
            subjectCombo.SelectedValue = null;

        }

        private void populateCombo()
        {
            var alist = new List<String>();
            alist.Add("Who cares");
            alist.Add("Important");
            alist.Add("Urgent");
            alist.Add("Global Crisis");

            subjectCombo.ItemsSource = alist;
        }

        /*End Programming Logic*/
    }
}
